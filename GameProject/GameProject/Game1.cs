using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameProject
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // game objects. Using inheritance would make this
        // easier, but inheritance isn't a GDD 1200 topic
        Burger burger;
        List<TeddyBear> bears = new List<TeddyBear>();
        static List<Projectile> projectiles = new List<Projectile>();
        List<Explosion> explosions = new List<Explosion>();

        // projectile and explosion sprites. Saved so they don't have to
        // be loaded every time projectiles or explosions are created
        static Texture2D frenchFriesSprite;
        static Texture2D teddyBearProjectileSprite;
        static Texture2D explosionSpriteStrip;

        // scoring support
        int score = 0;
        string scoreString = GameConstants.SCORE_PREFIX + 0;

        // health support
        string healthString = GameConstants.HEALTH_PREFIX +
            GameConstants.BURGER_INITIAL_HEALTH;
        bool burgerDead = false;

        // text display support
        SpriteFont font;

        // sound effects
        SoundEffect burgerDamage;
        SoundEffect burgerDeath;
        SoundEffect burgerShot;
        SoundEffect explosion;
        SoundEffect teddyBounce;
        SoundEffect teddyShot;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // set resolution
            graphics.PreferredBackBufferWidth = GameConstants.WINDOW_WIDTH;
            graphics.PreferredBackBufferHeight = GameConstants.WINDOW_HEIGHT;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            RandomNumberGenerator.Initialize();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // load audio content
            burgerDamage = this.Content.Load<SoundEffect>("sounds/BurgerDamage");
            burgerDeath = this.Content.Load<SoundEffect>("sounds/BurgerDeath");
            burgerShot = this.Content.Load<SoundEffect>("sounds/BurgerShot");
            explosion = this.Content.Load<SoundEffect>("sounds/Explosion");
            teddyBounce = this.Content.Load<SoundEffect>("sounds/TeddyBounce");
            teddyShot = this.Content.Load<SoundEffect>("sounds/TeddyShot");

            // load sprite font
            font = this.Content.Load<SpriteFont>("Arial20");

            // load projectile and explosion sprites
            frenchFriesSprite = this.Content.Load<Texture2D>("frenchfries");
            teddyBearProjectileSprite = this.Content.Load<Texture2D>("teddybearprojectile");
            explosionSpriteStrip = this.Content.Load<Texture2D>("explosion");


            // add initial game objects
            burger = new Burger(this.Content, "burger", (1 / 8) * Window.ClientBounds.Width, (1 / 8) * Window.ClientBounds.Height, burgerDamage);
            while (bears.Count < GameConstants.MAX_BEARS)
            {
                SpawnBear(); 
            }
            


            // set initial health and score strings          
            healthString = GameConstants.HEALTH_PREFIX + burger.Health;
            scoreString = GameConstants.SCORE_PREFIX + score;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            base.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            CheckBurgerKill();
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();

            // get current mouse state and update burger
      
            KeyboardState keyboardState = Keyboard.GetState();
            burger.Update(gameTime, keyboardState);

            // update other game objects
            foreach (TeddyBear bear in bears)
            {
                bear.Update(gameTime);
            }
            foreach (Projectile projectile in projectiles)
            {
                projectile.Update(gameTime);
            }
            foreach (Explosion explosion in explosions)
            {
                explosion.Update(gameTime);
            }
          

            // check and resolve collisions between teddy bears
            foreach (TeddyBear bear1 in bears)
            {
                for (int i = bears.IndexOf(bear1) +1; i < bears.Count ; i++)
                {
                    TeddyBear bear2 = bears[i];
                    CollisionResolutionInfo info = CollisionUtils.CheckCollision(gameTime.ElapsedGameTime.Milliseconds, GameConstants.WINDOW_WIDTH,
                        GameConstants.WINDOW_HEIGHT, bear1.Velocity, bear1.CollisionRectangle, bear2.Velocity, bear2.CollisionRectangle);
                    if (info != null)
                    {
                        if (info.FirstOutOfBounds)
                        {
                            bear1.Active = false;
                        }
                        else
                        {
                            bear1.Velocity = info.FirstVelocity;
                            bear1.DrawRectangle = info.FirstDrawRectangle;
                        }

                        if (info.SecondOutOfBounds)
                        {
                            bear2.Active = false;
                        }
                        else
                        {
                            bear2.Velocity = info.SecondVelocity;
                            bear2.DrawRectangle = info.SecondDrawRectangle;
                        }
                        
                        
                    }
                }
            }

            // check and resolve collisions between burger and teddy bears
            foreach (TeddyBear bear in bears) 
            {
                if (burger.CollisionRectangle.Intersects(bear.CollisionRectangle))
                {
                    Explosion explosion = new Explosion(explosionSpriteStrip, bear.CollisionRectangle.X + GameConstants.TEDDY_BEAR_PROJECTILE_OFFSET, bear.CollisionRectangle.Y + GameConstants.TEDDY_BEAR_PROJECTILE_OFFSET, this.explosion);
                    explosions.Add(explosion);
                    
                    bear.Active = false;
                    burger.Health -= GameConstants.BEAR_DAMAGE;
                    //burgerShot.Play(0.25f, 0, 0);
                }

            }
            

            // check and resolve collisions between burger and projectiles            

            foreach (Projectile p in projectiles)
            {
                if (burger.CollisionRectangle.Intersects(p.CollisionRectangle) && p.Type != ProjectileType.FrenchFries)
                {
                    p.Active = false;
                    burger.Health -= GameConstants.TEDDY_BEAR_PROJECTILE_DAMAGE;
                    healthString = GameConstants.HEALTH_PREFIX + burger.Health;
                    burgerShot.Play();                  
                    

                }
            }


            // check and resolve collisions between teddy bears and projectiles
            foreach (TeddyBear bear in bears)
            {
                foreach (Projectile p in projectiles)
                {
                    if (bear.CollisionRectangle.Intersects(p.CollisionRectangle))
                    {
                        if (p.Type == ProjectileType.FrenchFries)
                        {
                            bear.Active = false;
                            score += GameConstants.BEAR_POINTS;
                            p.Active = false;
                            Explosion explosionobj = new Explosion(explosionSpriteStrip, bear.CollisionRectangle.X + GameConstants.TEDDY_BEAR_PROJECTILE_OFFSET, bear.CollisionRectangle.Y + GameConstants.TEDDY_BEAR_PROJECTILE_OFFSET, this.explosion);
                            //explosion.Play(0.25f, 0, 0);
                            explosions.Add(explosionobj);
                            
                        }
                    }
                }

            }
            while (bears.Count < GameConstants.MAX_BEARS)
            {
                SpawnBear();
            }

            //// clean out inactive teddy bears and add new ones as necessary
            bears = bears.Where(x => x.Active == true).ToList();

            // //clean out inactive projectiles            
            projectiles = projectiles.Where(x => x.Active == true).ToList();

            //// clean out finished explosions
            explosions = explosions.Where(x => x.Finished == false).ToList();
            scoreString = GameConstants.SCORE_PREFIX + score;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            // draw game objects
            burger.Draw(spriteBatch);
            foreach (TeddyBear bear in bears)
            {
                bear.Draw(spriteBatch);
            }
            foreach (Projectile projectile in projectiles)
            {
                projectile.Draw(spriteBatch);
            }
            foreach (Explosion explosion in explosions)
            {
                explosion.Draw(spriteBatch);
            }

            // draw score and health
            spriteBatch.DrawString(font, healthString, GameConstants.HEALTH_LOCATION, Color.White);
            spriteBatch.DrawString(font, scoreString, GameConstants.SCORE_LOCATION, Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
            
        }

        #region Public methods

        /// <summary>
        /// Gets the projectile sprite for the given projectile type
        /// </summary>
        /// <param name="type">the projectile type</param>
        /// <returns>the projectile sprite for the type</returns>
        public static Texture2D GetProjectileSprite(ProjectileType type)
        {
            // replace with code to return correct projectile sprite based on projectile type
            if (type == ProjectileType.FrenchFries)
            {
                return frenchFriesSprite;
            }
            else
            {
                return teddyBearProjectileSprite;
            }

        }

        /// <summary>
        /// Adds the given projectile to the game
        /// </summary>
        /// <param name="projectile">the projectile to add</param>
        public static void AddProjectile(Projectile projectile)
        {
            projectiles.Add(projectile);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Spawns a new teddy bear at a random location
        /// </summary>
        private void SpawnBear()
        {
            // generate random location            
            int location_x = GetRandomLocation(GameConstants.SPAWN_BORDER_SIZE,
                graphics.PreferredBackBufferWidth - 2 * GameConstants.SPAWN_BORDER_SIZE);
            int location_y = GetRandomLocation(GameConstants.SPAWN_BORDER_SIZE,
                graphics.PreferredBackBufferHeight - 2 * GameConstants.SPAWN_BORDER_SIZE);

            // generate random velocity
            float speedX = GameConstants.MIN_BEAR_SPEED + (float)RandomNumberGenerator.Next((int)GameConstants.BEAR_SPEED_RANGE);
            float speedY = GameConstants.MIN_BEAR_SPEED + (float)RandomNumberGenerator.Next((int)GameConstants.BEAR_SPEED_RANGE);
            //float angle = (float)Math.PI * (float)RandomNumberGenerator.NextFloat((float)GameConstants.BEAR_SPEED_RANGE);



            // create new bear
            TeddyBear newBear = new TeddyBear(this.Content, "teddybear", location_x, location_y, new Vector2(speedX, speedY), teddyBounce, teddyShot);

            // make sure we don't spawn into a collision

            while (CollisionUtils.IsCollisionFree(newBear.CollisionRectangle, GetCollisionRectangles()) == false)
            {
                newBear.X = GetRandomLocation(GameConstants.SPAWN_BORDER_SIZE,
                graphics.PreferredBackBufferWidth - 2 * GameConstants.SPAWN_BORDER_SIZE);
                newBear.Y = GetRandomLocation(GameConstants.SPAWN_BORDER_SIZE,
                graphics.PreferredBackBufferHeight - 2 * GameConstants.SPAWN_BORDER_SIZE);
            }
            // add new bear to list
            bears.Add(newBear);
          

        }

        /// <summary>
        /// Gets a random location using the given min and range
        /// </summary>
        /// <param name="min">the minimum</param>
        /// <param name="range">the range</param>
        /// <returns>the random location</returns>
        private int GetRandomLocation(int min, int range)
        {
            return min + RandomNumberGenerator.Next(range);
        }

        /// <summary>
        /// Gets a list of collision rectangles for all the objects in the game world
        /// </summary>
        /// <returns>the list of collision rectangles</returns>
        private List<Rectangle> GetCollisionRectangles()
        {
            List<Rectangle> collisionRectangles = new List<Rectangle>();
            collisionRectangles.Add(burger.CollisionRectangle);
            foreach (TeddyBear bear in bears)
            {
                collisionRectangles.Add(bear.CollisionRectangle);
            }
            foreach (Projectile projectile in projectiles)
            {
                collisionRectangles.Add(projectile.CollisionRectangle);
            }
            foreach (Explosion explosion in explosions)
            {
                collisionRectangles.Add(explosion.CollisionRectangle);
            }
            return collisionRectangles;
        }

        /// <summary>
        /// Checks to see if the burger has just been killed
        /// </summary>
        private void CheckBurgerKill()
        {
            if (burger.Health == 0)
            {
                burgerDead = true;
                burgerDeath.Play();
                
            }
            if (burger.Health < 0)
            {
                burger.Health = 0;
            }
            
                
        }

        #endregion
    }
}
